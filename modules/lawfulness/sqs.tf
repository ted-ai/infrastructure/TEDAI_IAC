resource "aws_sqs_queue" "new_notices" {
  name                       = "${var.resource_prefix}-new-notices"
  visibility_timeout_seconds = 3600
  sqs_managed_sse_enabled    = true
  tags                       = var.tags
}

data "aws_iam_policy_document" "new_notices" {
  statement {
    effect    = "Allow"
    actions   = ["sqs:SendMessage"]
    resources = [aws_sqs_queue.new_notices.arn]

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    condition {
      test     = "ArnEquals"
      variable = "aws:SourceArn"
      values   = [var.input_bucket_arn]
    }
  }
}

resource "aws_sqs_queue_policy" "new_notices" {
  queue_url = aws_sqs_queue.new_notices.id
  policy    = data.aws_iam_policy_document.new_notices.json
}

resource "aws_dynamodb_table" "flagged_notices" {
  name         = "${var.resource_prefix}-flagged-notices"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "notice_id"
  tags         = var.tags

  attribute {
    name = "notice_id"
    type = "S"
  }

  server_side_encryption {
    enabled = true
  }
}

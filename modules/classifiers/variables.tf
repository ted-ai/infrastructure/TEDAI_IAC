variable "tags" {
  type = map(string)
}

variable "sagemaker_classifiers_execution_role_name_prefix" {
  type = string
}

variable "sagemaker_classifier_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_multi_label_division_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_multi_label_division_classifier_model_url" {
  type = string
}

variable "ssm_classifier_endpoint_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_opentender_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_opentender_multi_label_division_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_opentender_multi_label_division_classifier_model_url" {
  type = string
}

variable "ssm_classifier_endpoint_opentender_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_ss_multi_label_cpv_classifier_name" {
  type = string
}

variable "sagemaker_classifier_ss_multi_label_cpv_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_ss_multi_label_cpv_classifier_model_url" {
  type = string
}

variable "ssm_classifier_endpoint_ss_multi_label_cpv_classifier_name" {
  type = string
}

variable "sagemaker_classifier_budgetary_value_classifier_name" {
  type = string
}

variable "sagemaker_classifier_budgetary_value_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_budgetary_value_classifier_model_url" {
  type = string
}

variable "ssm_classifier_endpoint_budgetary_value_classifier_name" {
  type = string
}

variable "sagemaker_classifier_roberta_multi_label_division_classifier_name" {
  type = string
}

variable "sagemaker_classifier_roberta_multi_label_division_classifier_image_url" {
  type = string
}

variable "sagemaker_classifier_roberta_multi_label_division_classifier_model_url" {
  type = string
}

variable "ssm_classifier_endpoint_roberta_multi_label_division_classifier_name" {
  type = string
}

variable "applications_dashboard_docker_image_url" {
  type = string
}

variable "dashboard_port" {
  type = number
}

variable "lawfulness_host" {
  type = string
}

# Network
variable "vpc_id" {
  type = string
}

variable "private_subnet_id_list" {
  type = list(string)
}

variable "private_subnet_id_az1_list" {
  type = list(string)
}

variable "private_subnet_id_az2_list" {
  type = list(string)
}

# IAM
variable "iam_role_prefix" {
  type = string
}

variable "iam_policy_prefix" {
  type = string
}

variable "project_account_id" {
  type = string
}
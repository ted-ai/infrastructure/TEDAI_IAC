resource "aws_ecr_repository" "sagemaker_classifiers" {
  name                 = var.ecr_repository_sagemaker_classifiers_name
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = var.tags
}

resource "aws_ecr_repository" "sagemaker_classifiers_ci" {
  name                 = "ci-temporary-images"
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
  tags = var.tags
}

resource "aws_ecr_lifecycle_policy" "sagemaker_classifiers_ci" {
  repository = aws_ecr_repository.sagemaker_classifiers_ci.name

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire images older than 1 days",
            "selection": {
                "tagStatus": "any",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 1
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}

resource "aws_ecr_repository" "applications" {
  name                 = var.ecr_repository_application_name
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }

  tags = var.tags
}

data "aws_iam_policy_document" "sagemaker_ci_training" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["sagemaker.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "sagemaker_ci_training" {
  name = "${var.sagemaker_classifiers_training_role_name_prefix}-policy"
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["s3:ListAllMyBuckets", "s3:ListBucket", "s3:HeadBucket"]
        Effect   = "Allow"
        Resource = "*"
      },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetObjectAttributes",
                "s3:ListBucket",
                "s3:DeleteObject"
            ],
            "Resource": [
                aws_s3_bucket.data_bucket.arn,
                "${aws_s3_bucket.data_bucket.arn}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:GetObjectAttributes",
                "s3:ListBucket"
            ],
            "Resource": [
                aws_s3_bucket.data_bucket.arn,
                "${aws_s3_bucket.data_bucket.arn}/*",
                aws_s3_bucket.ml_models_bucket.arn,
              "${aws_s3_bucket.ml_models_bucket.arn}/*"
            ]
        }
    ]
  })
}

resource "aws_iam_role" "sagemaker_ci_training" {
  name                 = "${var.sagemaker_classifiers_training_role_name_prefix}-role"
  assume_role_policy   = data.aws_iam_policy_document.sagemaker_ci_training.json
  managed_policy_arns  = [aws_iam_policy.sagemaker_ci_training.arn, "arn:aws:iam::aws:policy/AmazonSageMakerFullAccess"]
  permissions_boundary = "arn:aws:iam::${var.project_account_id}:policy/Team_Admin_Boundary"
}
